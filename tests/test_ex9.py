import unittest
from exercicios import ex9


class TestEX9(unittest.TestCase):
    def test_faturamento(self):
        expected_tot_mg = 6.95
        expected_tot_cal = 3.50
        expected_tot_caneta = 1.20
        expected_tot = 11.649999999999999

        tot_mg, tot_cal, tot_caneta, tot = ex9.faturamento(1, 1, 1)
        self.assertEqual(expected_tot_mg, tot_mg)
        self.assertEqual(expected_tot_cal, tot_cal)
        self.assertEqual(expected_tot_caneta, tot_caneta)
        self.assertEqual(expected_tot, tot)
