import unittest
from exercicios import ex10


class TestEX10(unittest.TestCase):
    def test_calcula_imposto(self):
        salario = 4000
        expected_desc = 900
        expected_salario_liq = 3100.0

        resultado = ex10.calcula_imposto(salario)
        self.assertEqual((expected_desc, expected_salario_liq), resultado)
