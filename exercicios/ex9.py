def faturamento(mg, cal, caneta):
    tot_mg = mg * 6.95
    tot_cal = cal * 3.50
    tot_caneta = caneta * 1.20
    tot = tot_mg + tot_cal + tot_caneta
    return tot_mg, tot_cal, tot_caneta, tot


if __name__ == "__main__":
    mg = int(input("Quantos mini-games foram vendidos hoje ? : "))
    cal = int(input("Quantos calculadoras foram vendidos hoje ? : "))
    caneta = int(input("Quantos canetas foram vendidos hoje ? : "))

    tot_mg, tot_cal, tot_caneta, tot = faturamento(mg, cal, caneta)
    print("Faturamento total de Mini-Games:", tot_mg)
    print("Faturamento total de calculadoras:", tot_cal)
    print("Faturamento total de canetas:", tot_caneta)
    print("Faturamento total foi de:", tot)
