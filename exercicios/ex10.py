def calcula_imposto(salario_bruto):
    salario_liq = 0.0
    desc = 0.0
    if salario_bruto <= 2826.65:
        desc = salario_bruto * 0.075
        salario_liq = salario_bruto - desc
    elif salario_bruto <= 3715.05:
        desc = salario_bruto * 0.15
        salario_liq = salario_bruto - desc
    elif salario_bruto <= 4664.68:
        desc = salario_bruto * 0.225
        salario_liq = salario_bruto - desc
    else:
        desc = salario_bruto * 0.275
        salario_liq = salario_bruto - desc

    return desc, salario_liq


if __name__ == "__main__":
    salario = float(input("Digite seu salário bruto: "))

    desc, salario_liq = calcula_imposto(salario)
    print("O salário bruto é: ", salario)
    print("O desconto correspondente é: ", desc)
    print("O salário líquido é : ", salario_liq)
